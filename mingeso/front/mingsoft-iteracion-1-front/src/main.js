import Vue from 'vue'
import router from './router'
import store from './store'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import i18n from './lang'
import numeral from 'numeral'

Vue.config.productionTip = false

Vue.filter('formatNumber', (value) => {
  return numeral(value).format('0,0')
})

Vue.filter('formatPhoneNumber', (phoneNumberString) => {
  var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
  var match = cleaned.match(/(\d{1})(\d{4})(\d{4})$/)
  if (match) {
    return [match[1], ' ', match[2], '-', match[3]].join('')
  }
  return null
})

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: (h) => h(App),
}).$mount('#app')
