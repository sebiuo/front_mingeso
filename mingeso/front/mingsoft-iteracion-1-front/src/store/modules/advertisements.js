import axios from 'axios'
import Vue from 'vue'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'

Vue.use(VueToast);

const headers = {
  'x-api-key': '436236939443955C11494D448451F',
  'Content-Type': 'application/json'
}

const advertisements = {
  namespaced: true,
  state: {
    advertisement: [],
    advertisements: [],
    userAdvertisements: [],
    advertisementsFetching: 'NO_FETCH',
    userAdvertisementsFetching: 'NO_FETCH',
    advertisementFetching: 'NO_FETCH'
  },
  mutations: {
    // ADVERTISEMENT
    GET_ADVERTISEMENT_START(state) {
      state.advertisementFetching = 'FETCHING'
    },
    GET_ADVERTISEMENT_SUCCESS(state, data) {
      state.advertisement = data,
      state.advertisementFetching = 'FETCHED'
    },
    GET_ADVERTISEMENT_ERROR(state) {
      state.advertisementFetching = 'ERROR'
    },

    // ALL ADVERTISEMENTS
    GET_ADVERTISEMENTS_START(state) {
      state.advertisementsFetching = 'FETCHING'
    },
    GET_ADVERTISEMENTS_SUCCESS(state, data) {
      state.advertisements = data,
      state.advertisementsFetching = 'FETCHED'
    },
    GET_ADVERTISEMENTS_ERROR(state) {
      state.advertisementsFetching = 'ERROR'
    },
    
    // USER ADVERTISEMENTS
    GET_USER_ADVERTISEMENTS_START(state) {
      state.userAdvertisementsFetching = 'FETCHING'
    },
    GET_USER_ADVERTISEMENTS_SUCCESS(state, data) {
      state.userAdvertisements = data,
      state.userAdvertisementsFetching = 'FETCHED'
    },
    GET_USER_ADVERTISEMENTS_ERROR(state) {
      state.userAdvertisementsFetching = 'ERROR'
    },

    // POST DATOS FILTRADOS
    POST_DATA_FILTER_SUCCESS(state, data) {
      state.advertisements = data
    }
  },
  actions: {
    async getAdvertisement({ commit }, data) {
      commit('GET_ADVERTISEMENT_START')
      await axios
              .post('http://159.203.106.137:5000/api/v1/core/seleccionarAnunziao', data, { headers })
              .then((result) => {
                commit('GET_ADVERTISEMENT_SUCCESS', result.data.anunziao)
              })
              .catch((e) => {
                console.error(e)
                commit('GET_ADVERTISEMENT_ERROR')
              })
    },
    async getAdvertisements({ commit }) {
      commit('GET_ADVERTISEMENTS_START')
      await axios
              .post('http://159.203.106.137:5000/api/v1/core/obtenerAnunziaos', '', { headers })
              .then((result) => {
                commit('GET_ADVERTISEMENTS_SUCCESS', result.data.anunziaos)
              })
              .catch((e) => {
                console.error(e)
                commit('GET_ADVERTISEMENTS_ERROR')
              })
    },
    async getUserAdvertisements({ commit }) {
      commit('GET_USER_ADVERTISEMENTS_START')
      await axios
              .post('http://159.203.106.137:5000/api/v1/core/GetUserAnunziaos', { headers })
              .then(result => {
                commit('GET_USER_ADVERTISEMENTS_SUCCESS', result.data)
              })
              .catch((e) => {
                console.error(e)
                commit('GET_USER_ADVERTISEMENTS_ERROR')
              })
    },
    async postDatosFiltrados({ commit }, data) {
      commit('POST_DATA_FILTER_SUCCESS', data)
      Vue.$toast.info('Datos filtrados obtenidos exitosamente!', {
        position: 'top-right'
      })
    }
  },
}

export default advertisements
