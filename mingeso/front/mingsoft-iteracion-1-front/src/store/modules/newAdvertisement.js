import axios from 'axios'

const headers = {
  'x-api-key': '436236939443955C11494D448451F',
  'Content-Type': 'application/json'
}

const newAdvertisement = {
  namespaced: true,
  state: {
    advertisement: [],
    categories: [],
    fetching: 'NO_FETCH',
    categoriesFetching: 'NO_FETCH'
  },
  mutations: {
    POST_ADVERTISEMENT_START(state) {
      state.fetching = 'FETCHING'
    },
    POST_ADVERTISEMENT_SUCCESS(state, data) {
      state.advertisement = data,
      state.fetching = 'FETCHED'
    },
    POST_ADVERTISEMENT_ERROR(state) {
      state.fetching = 'ERROR'
    },
    // CATEGORIES
    GET_CATEGORIES_START(state) {
      state.categoriesFetching = 'FETCHING'
    },
    GET_CATEGORIES_SUCCESS(state, data) {
      state.categories = data,
      state.categoriesFetching = 'FETCHED'
    },
    GET_CATEGORIES_ERROR(state) {
      state.categoriesFetching = 'ERROR'
    }
  },
  actions: {
    async postNewAdvertisement({ commit }, data) {
      commit('POST_ADVERTISEMENT_START')
      await axios
              .post('http://159.203.106.137:5000/api/v1/core/nuevoAnuncio', data, { headers })
              .then((result) => {
                commit('POST_ADVERTISEMENT_SUCCESS', result.data)
              })
              .catch((e) => {
                console.error(e)
                commit('POST_ADVERTISEMENT_ERROR')
              })
    },
    async getCategories({ commit }) {
      commit('GET_CATEGORIES_START')
      await axios
              .get('http://159.203.106.137:5000/api/v1/core/obtenerCategorias', { headers })
              .then((result) => {
                commit('GET_CATEGORIES_SUCCESS', result.data.categoria)
              })
              .catch((e) => {
                console.error(e)
                commit('GET_CATEGORIES_ERROR')
              })
    }
  }
}

export default newAdvertisement
