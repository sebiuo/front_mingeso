import axios from 'axios'
import Vue from 'vue'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'
import { Role } from '../../_helpers/role'

Vue.use(VueToast);

const headers = {
  'x-api-key': '436236939443955C11494D448451F',
  'Content-Type': 'application/json'
}

const user = {
  namespaced: true,
  state : {
    user: [],
    signUpFetching: 'NO_FETCH',
    logInFetching: 'NO_FETCH',
  },
  mutations: {
    // SIGNUP
    SIGNUP_USER_START(state) {
      state.signUpFetching = 'FETCHING'
    },
    SIGNUP_USER_SUCCESS(state, data) {
      state.user = data,
      state.signUpFetching = 'FETCHED'
    },
    SIGNUP_USER_ERROR(state) {
      state.signUpFetching = 'ERROR'
    },
    // LOGIN
    LOGIN_USER_START(state) {
      state.logInFetching = 'FETCHING'
    },
    LOGIN_USER_SUCCESS(state, data) {
      console.log(data, 'DATA')
      state.user = data,
      state.logInFetching = 'FETCHED'
    },
    LOGIN_USER_ERROR(state) {
      state.logInFetching = 'ERROR'
    },
    // LOGOUT
    LOGOUT_USER_SUCCESS(state) {
      state.user = [],
      state.signUpFetching = 'NO_FETCH',
      state.logInFetching = 'NO_FETCH'
    }
  },
  actions: {
    async signUpUser({ commit }, data) {
      commit('SIGNUP_USER_START')
      await axios
              .post('http://159.203.106.137:5000/api/v1/core/inscripcionCiudadano', data, { headers })
              .then((result) => {
                commit('SIGNUP_USER_SUCCESS', result.data[0])
                Vue.$toast.info('Usuario registrado exitosamente', {
                  position: 'top-right'
                })
              })
              .catch((e) => {
                console.error(e)
                commit('SIGNUP_USER_ERROR')
                Vue.$toast.error('No se pudo registrar', {
                  position: 'top-right'
                })
              })
    },
    async logInUser({ commit }, data, stayConnected ) {
      commit('LOGIN_USER_START')
      await axios
              .post('http://159.203.106.137:5000/api/v1/core/inicioSesion', data, { headers })
              .then((result) => {
                if(stayConnected) {
                  localStorage.setItem('session', JSON.stringify(result.data))
                  localStorage.setItem('login', JSON.stringify(data))
                  //sessionStorage.setItem('authorization', result.data[0].authorization)
                } else {
                  sessionStorage.setItem('session', JSON.stringify(result.data))
                  sessionStorage.setItem('login', JSON.stringify(data))
                  //sessionStorage.setItem('authorization', result.data[0].authorization)
                }

                commit('LOGIN_USER_SUCCESS', result.data)
                Vue.$toast.info('Usuario logeado exitosamente', {
                  position: 'top-right'
                })
                if(result.data.rol === Role.JuntaVecinal) {
                  //this.$router.push('/sign-up')
                } else {
                  //this.$router.push('/')
                }
              })
              .catch((e) => {
                console.error(e)
                commit('LOGIN_USER_ERROR')
                Vue.$toast.error('No se pudo iniciar sesion', {
                  position: 'top-right'
                })
              })
    },
    async logOutUser({ commit }) {
      commit('LOGOUT_USER_SUCCESS')
      localStorage.clear()
      sessionStorage.clear()
      Vue.$toast.info('Cerrado de sesion exitosamente', {
        position: 'top-right'
      })
    }
  }
}

export default user