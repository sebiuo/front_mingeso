import axios from 'axios'

const headers = {
  'x-api-key': '436236939443955C11494D448451F',
  'Content-Type': 'application/json'
}

const promotion = {
  namespaced: true,
  state: {
    result: [],
    promotionFetching: 'NO_FETCH',
  },
  mutations: {
    // PROMOTION
    POST_PROMOTION_START(state) {
      state.promotionFetching = 'FETCHING'
    },
    POST_PROMOTION_SUCCESS(state, data) {
      state.result = data,
      state.promotionFetching = 'FETCHED'
    },
    POST_PROMOTION_ERROR(state) {
      state.promotionFetching = 'ERROR'
    }
  },
  actions: {
    async postPromotion({ commit }, data) {
      commit('POST_PROMOTION_START')
      await axios
              .post('http://159.203.106.137:5000/api/v1/core/crearPromocion', data, { headers })
              .then((result) => {
                commit('POST_PROMOTION_SUCCESS', result.data)
              })
              .catch((e) => {
                console.error(e)
                commit('POST_PROMOTION_ERROR')
              })
    }
  },
}

export default promotion