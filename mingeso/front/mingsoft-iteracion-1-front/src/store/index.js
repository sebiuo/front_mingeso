import Vue from 'vue'
import axios from 'axios'
import Vuex from 'vuex'
import advertisements from './modules/advertisements'
import user from './modules/user'
import newAdvertisement from './modules/newAdvertisement'
import comment from './modules/comment'
import promotion from './modules/promotion'

Vue.use(Vuex, axios)

export default new Vuex.Store({
  modules: {
    advertisements,
    user,
    newAdvertisement,
    comment,
    promotion
  },
})
